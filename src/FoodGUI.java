import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextArea orderedItemsList;
    private JButton SobaButton;
    private JButton sushiButton;
    private JButton sukiyakiButton;
    private JLabel sum;
    private JButton CheckOutButton;

    int sumprice = 0;


    void order(String food , int price) {
    int confirmation = JOptionPane.showConfirmDialog(null,
            "Would you like to order " + food + "?",
            "Order Confirmation",
            JOptionPane.YES_NO_OPTION);
    if (confirmation == 0) {
        int bigconfirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to add a large size for 150 yen?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        String currentText = orderedItemsList.getText();
        orderedItemsList.setText(currentText + food + " " + price + "yen" + "\n");

        if (bigconfirmation == 0) {
            String bigcurrentText = orderedItemsList.getText();
            orderedItemsList.setText(bigcurrentText + " +large size 150 yen" + "\n");
            sumprice += 150;
        }
        else{
            int smallconfirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to add a small size for an additional -100 yen?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION);

            if (smallconfirmation == 0) {
                String scurrentText = orderedItemsList.getText();
                orderedItemsList.setText(scurrentText + " +small size -100 yen" + "\n");
                sumprice += -100;
            }
        }
        JOptionPane.showMessageDialog(null, "Order for " + food + " received.");

        sumprice += price;
        sum.setText("Total:" + " " + sumprice + "yen");
    }
    else {
        JOptionPane.showMessageDialog(null, "Canceled " + food + " order.");
    }
}

    public FoodGUI() {

        String icon;
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("food_tempura.png")));
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",680);
            }
        });

        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.png")));
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",590);
            }
        });

        udonButton.setIcon(new ImageIcon(this.getClass().getResource("udon.png")));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",390);
            }
        });

        SobaButton.setIcon(new ImageIcon(this.getClass().getResource("Soba.png")));
        SobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba",490); }
        });

        sushiButton.setIcon(new ImageIcon(this.getClass().getResource("sushi.png")));
        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi",800); }
        });

        sukiyakiButton.setIcon(new ImageIcon(this.getClass().getResource("sukiyaki.png")));
        sukiyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sukiyaki",1200); }
        });
        CheckOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout ?",
                        "Check Out Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you." + "The total price is" + sumprice + "yen");
                    orderedItemsList.setText("");
                    sumprice=0;
                    sum.setText("Total:" + " " + sumprice + "yen");

                }

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private record addListModel() {
    }
}
